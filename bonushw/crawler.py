import urllib.request
from urllib.request import urlopen
from bs4 import BeautifulSoup
from urllib.parse import urljoin
import time
import re
import hashlib
import errno    
import os
import sys

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def crawl(seeds, crawlLimit):
    frontier = seeds
    visitedUrls = set()
    mstRegex = re.compile('.*mst.edu.*')
    mkdir_p('pages')
    
    numLinksCrawled = 0
    for url in frontier:
        print('Crawling', url)
        visitedUrls.add(url)

        try:
            res = urllib.request.urlopen(url)
        except:
            print('Could not access', url)
            continue

        contentType = res.info().get('Content-Type')

        if not contentType.startswith('text/html'):
            print('Skipping %s content' % contentType)
            continue

        contents = str(res.read(), encoding='utf8')

        filename='pages/' + hashlib.md5(url.encode()).hexdigest() + '.html'
        file = open(filename, 'w')
        file.write(contents)
        file.close()
        soup = BeautifulSoup(contents, 'lxml')

        discoveredUrls = set()
        links = soup('a')
        for link in links:
            if ('href' in dict(link.attrs)):
                newUrl = urljoin(url, link['href'])
                if (not mstRegex.match(newUrl)):
                    # print('%s is not an mst.edu website' % newUrl)
                    continue
                if (newUrl[0:4] == 'http' and newUrl not in visitedUrls and newUrl not in discoveredUrls and newUrl not in frontier):
                    discoveredUrls.add(newUrl)
        
        numLinksCrawled += 1
        if (numLinksCrawled >= crawlLimit):
            break

        frontier += discoveredUrls
        time.sleep(1)
    
    print('Crawling Finished')

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: %s <crawlLimit>' % sys.argv[0])
    crawl(['https://www.mst.edu'], int(sys.argv[1]))