import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression, Lasso, Ridge, ElasticNet
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV

titles = ['median_house_value','median_income','housing_median_age','total_rooms','total_bedrooms','population','households','latitude','longitude']
target = titles[0]

df = pd.read_csv('cadata.csv')

for title in titles:
    plt.boxplot(df[title])
    plt.title(title)
    plt.show()
    if title != target:
        plt.scatter(df[target], df[title])
        plt.title(title + " and " + target)
        plt.show()

# Split data into features and target dataframes

df_x = df.drop([target],axis=1)
df_y = df[target]

train_x, test_x, train_y, test_y = train_test_split(df_x, df_y, test_size=0.2)

# Train the models
print("\nNormal Data\n")
lin_reg = LinearRegression()
lin_reg.fit(train_x, train_y)
print(lin_reg.score(train_x, train_y))

rid_reg = Ridge()
rid_reg.fit(train_x, train_y)
print(rid_reg.score(train_x, train_y))

las_reg = Lasso()
las_reg.fit(train_x, train_y)
print(las_reg.score(train_x, train_y))

eln_reg = ElasticNet()
eln_reg.fit(train_x, train_y)
print(eln_reg.score(train_x, train_y))

# Scaled Training
print("\nStandard Scaled Data\n")
scaler = StandardScaler()
scaled_x = scaler.fit_transform(train_x)

lin_reg = LinearRegression()
lin_reg.fit(scaled_x, train_y)
print(lin_reg.score(scaled_x, train_y))

rid_reg = Ridge()
rid_reg.fit(scaled_x, train_y)
print(rid_reg.score(scaled_x, train_y))

las_reg = Lasso()
las_reg.fit(scaled_x, train_y)
print(las_reg.score(scaled_x, train_y))

eln_reg = ElasticNet()
eln_reg.fit(scaled_x, train_y)
print(eln_reg.score(scaled_x, train_y))

# Grid Search Stuff
print("\nGrid Search Data\n")
lin_reg = LinearRegression()
rid_reg = Ridge()
las_reg = Lasso()
eln_reg = ElasticNet()

tuned_parameters = [
    {'fit_intercept': [True, False], 'normalize': [True, False], 'copy_X': [True, False]}
]

lin_reg = GridSearchCV(lin_reg, tuned_parameters, cv=5)

tuned_parameters = [
    {'alpha': [0.0, 1.0], 'fit_intercept': [True, False], 'normalize': [True, False], 'copy_X': [True, False]}
]

rid_reg = GridSearchCV(rid_reg, tuned_parameters, cv=5)

tuned_parameters = [
    {'alpha': [0.1, 1.0]}
]

las_reg = GridSearchCV(las_reg, tuned_parameters, cv=5)

tuned_parameters = [
    {'random_state': [1, None]}
]

eln_reg = GridSearchCV(eln_reg, tuned_parameters, cv=5)

titles = ['median_income','housing_median_age','total_rooms','total_bedrooms','population','households','latitude','longitude']


lin_reg.fit(train_x, train_y)
print(lin_reg.score(train_x, train_y))
print("Coefficients:", lin_reg.best_estimator_.coef_)

plt.scatter(titles, lin_reg.best_estimator_.coef_)
plt.show()

rid_reg.fit(train_x, train_y)
print(rid_reg.score(train_x, train_y))
print("Coefficients:", rid_reg.best_estimator_.coef_)

plt.scatter(titles, rid_reg.best_estimator_.coef_)
plt.show()

las_reg.fit(train_x, train_y)
print(las_reg.score(train_x, train_y))
print("Coefficients:", las_reg.best_estimator_.coef_)

plt.scatter(titles, las_reg.best_estimator_.coef_)
plt.show()

eln_reg.fit(train_x, train_y)
print(eln_reg.score(train_x, train_y))
print("Coefficients:", eln_reg.best_estimator_.coef_)

plt.scatter(titles, eln_reg.best_estimator_.coef_)
plt.show()