
# coding: utf-8

# In[60]:


import pandas as pd
import numpy as np

userprofile_df = pd.read_csv('./data/userprofile.csv')
userpayment_df = pd.read_csv('./data/userpayment.csv')
usercuisine_df = pd.read_csv('./data/usercuisine.csv')

userprofile_df = userprofile_df.drop(['latitude', 'longitude', 'weight', 'height', 'birth_year', 'transport', 'hijos', 'color'], axis=1) 

# one hot encodes the data
one_hot_cuisine = pd.get_dummies(usercuisine_df['Rcuisine'])
usercuisine_df = usercuisine_df.drop(['Rcuisine'], axis=1)
# joins the one hot encoded data and then aggregates it
usercuisine_df = usercuisine_df.join(one_hot_cuisine).groupby('userID').max()

# one hot encode the payment type
one_hot_payment = pd.get_dummies(userpayment_df['Upayment'])
userpayment_df = userpayment_df.drop(['Upayment'], axis=1)
userpayment_df = userpayment_df.join(one_hot_payment).groupby('userID').max()

# join the dataframes all together into one dataframe
user_df = pd.merge(pd.merge(userprofile_df, userpayment_df, on='userID'), usercuisine_df, on='userID')

for feature in ['smoker', 'drink_level', 'interest', 'personality', 'religion', 'budget', 'dress_preference', 'marital_status', 'ambience', 'activity']:
    one_hot_userprofile = pd.get_dummies(user_df[feature])
    one_hot_userprofile.rename(columns=lambda x: feature + '_' + x, inplace=True)
    user_df = user_df.drop([feature], axis=1)
    user_df = user_df.join(one_hot_userprofile).groupby('userID').max()

user_df


# In[61]:


from sklearn.naive_bayes import BernoulliNB

clf = BernoulliNB()


# In[46]:


chefmozparking_df = pd.read_csv('./data/chefmozparking.csv')
chefmozcusine_df = pd.read_csv('./data/chefmozcuisine.csv')
chefmozhours4_df = pd.read_csv('./data/chefmozhours4.csv')
chefmozaccepts_df = pd.read_csv('./data/chefmozaccepts.csv')
geoplaces2_df = pd.read_csv('./data/geoplaces2.csv', encoding = "ISO-8859-1")

# one hot encodes the restaurant cuisine data
one_hot_rcuisine = pd.get_dummies(chefmozcusine_df['Rcuisine'])
restaurantcuisine_df = chefmozcusine_df.drop(['Rcuisine'], axis=1)
restaurantcuisine_df = restaurantcuisine_df.join(one_hot_rcuisine).groupby('placeID').max()

# one hot encode the payment type accepted
one_hot_rpayment = pd.get_dummies(chefmozaccepts_df['Rpayment'])
restaurantpayment_df = chefmozaccepts_df.drop(['Rpayment'], axis=1)
restaurantpayment_df = restaurantpayment_df.join(one_hot_rpayment).groupby('placeID').max()

# one hot encode the parking
one_hot_parking = pd.get_dummies(chefmozparking_df['parking_lot'])
restaurantparking_df = chefmozparking_df.drop('parking_lot', axis = 1)
restaurantparking_df = restaurantparking_df.join(one_hot_parking).groupby('placeID').max()

# dropping:
#   - lat, long, the_geo_meter, zip, and address because they don't really mean much
#   - name because i dont think that affects the choice, 
#   - fax because that doesnt make sense to have
#   - other_services because it is mostly nones
geoplaces2_df = geoplaces2_df.drop(['latitude', 'longitude', 'the_geom_meter', 'name', 'address', 'fax', 'zip', 'other_services','url', 'city', 'state', 'country', 'franchise'], axis=1)


for feature in ['alcohol', 'price', 'dress_code', 'smoking_area', 'accessibility', 'Rambience', 'area']:
    one_hot_geoplaces = pd.get_dummies(geoplaces2_df[feature])
    geoplaces2_df = geoplaces2_df.drop([feature], axis=1)
    geoplaces2_df = geoplaces2_df.join(one_hot_geoplaces).groupby('placeID').max()


geoplaces2_df


# In[31]:


from sklearn import preprocessing 
from sklearn.cluster import KMeans
# have to normalize because sklearn doesn't do cosine similarity
# normalizing with euclidean has a linear relationship with cosine similarity though
X_Norm = preprocessing.normalize(geoplaces2_df)
kmeans = KMeans(n_clusters=8,init='random').fit(X_Norm)
kmeans.labels_

