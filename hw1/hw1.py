import random
import math

##### TASK 1 #####
# My programming environment is set up with anaconda

##### TASK 2 #####

# a)
def merge(first, second):
    sorted = []
    while (len(first) > 0 and len(second) > 0):
        if (first[0] > second[0]):
            sorted.append(second.pop(0))
        else:
            sorted.append(first.pop(0))
    
    while (len(first) > 0):
        sorted.append(first.pop(0))
    
    while (len(second) > 0):
        sorted.append(second.pop(0))

    return sorted

def mergeSort(array):
    if len(array) == 1:
        return array
    mid = int((len(array)) / 2)
    first = mergeSort(array[:mid])
    second = mergeSort(array[mid:])
    return merge(first, second)


a = [random.randint(0,9) for _ in range(10)]
b = mergeSort(a)
print('mergeSort is', 'correct' if b == sorted(b) else 'incorrect')

# b)

def max(array):
    if (len(array) == 0):
        return
    m = array[0]
    for i in array:
        if i > m:
            m = i
    return m

def min(array):
    if (len(array) == 0):
        return
    m = array[0]
    for i in array:
        if i < m:
            m = i
    return m

def mean(array):
    return sum(array) / len(array)

def stdev(array):
    m = mean(array)
    diff = [(x - m)**2 for x in array]
    return math.sqrt(mean(diff))

def percentile(array, p):
    r = (p / 100) * (len(array) + 1)
    return array[int(r)]

def summaryStatistics(array):
    ss = [max(array), min(array), mean(array), percentile(array, 50), percentile(array, 25), percentile(array, 75), stdev(array)]
    print("max:", ss[0])
    print("min:", ss[1])
    print("mean:", ss[2])
    print("stdev:", ss[3])
    print("median:", ss[4])
    print("25th Q:", ss[5])
    print("75th Q:", ss[6])
    return ss

# Generate three sets of random data
listA = [random.randint(0, 9) for _ in range(1000)]
listB = [random.gauss(5, 3) for _ in range(1000)]
listC = [math.exp(random.gauss(1, 0.5)) for _ in range(1000)]

# Complete the following code to calculate and plot summary statistics of  
print ("\nSummary Statistics A:")       
ssA = summaryStatistics(listA)
print ("\nSummary Statistics B:")       
ssB = summaryStatistics(listB)
print ("\nSummary Statistics C:")       
ssC = summaryStatistics(listC) 

# c)
def myHistWithRescale(array):
    array.sort()
    m = min(array)
    mx = max(array)
    dx = float(mx - m) / 10
    count = {}
    for i in range(0,10):
        count[i] = 0
    
   
    bucket = 0
    
    for x in array:
        while 1:
            if x >= m and x < (m + dx):
                count[bucket] += 1
                break
            elif x >= mx:
                count[bucket] += 1
                break
            else:
                m = m + dx
                bucket += 1
    
    return count

countA = myHistWithRescale(listA)
print("Count A:", countA)
countB = myHistWithRescale(listB)
print("Count B:", countB)
countC = myHistWithRescale(listC)
print("Count C:", countC)

import matplotlib.pyplot as plt

names = ["Data" + str(x) for x in range(1,4)]
symbols = ["v", "^", "+", "x", "^", "v"]
colors = ["black", "black", "blue", "blue", "red", "red"]

markers = []

for i in range(0, 6):
    markers.append(plt.scatter(names, [ssA[i], ssB[i], ssC[i]], c=colors[i], marker=symbols[i]))

markers.append(plt.scatter(names, [ssA[2] + ssA[6], ssB[2] + ssB[6], ssB[2] + ssA[6]], c="green", marker="v"))
markers.append(plt.scatter(names, [ssA[2] - ssA[6], ssB[2] - ssB[6], ssB[2] - ssA[6]], c="green", marker="^"))

plt.legend((x for x in markers),("max", "min", "mean", "median", "25th Quartile", "75th Quartile", "mean + stdev", "mean - stdev"),
        bbox_to_anchor=(.85, 1),
        loc='upper right',
        ncol=1,
        fontsize=8)

plt.show()

counts = [countA, countB, countC]
markers = ["o", "+", "x"]
colors = ["r", "g", "b"]
linestyles = ["-", "-.", ":"]

for i in range(0,3):
    names = list(counts[i].keys())
    values = list(counts[i].values())
    plt.plot(names, values, marker=markers[i], color=colors[i], linestyle=linestyles[i], label="Rescaled data " + str(i + 1))

plt.legend(loc="upper right", ncol=1, fontsize=8)
plt.xlabel("Values")
plt.ylabel("Frequency")

plt.show()

### Task 4 ###

import numpy as np

x = np.array([[ 1,2,3,4],[ 5,6,7,8],[ 9,10,11,12],[13,14,15,16]])

y = x[:, 2]; print (y)
y = x[-1,:2]; print (y)
y = x[:, [True, False, False, True]]; print(y)
y = x[0:2, 0:2]; print(y)
y = x[[0, 1, 2], [0, 1, 2]]; print(y)
y = x[0]**2; print(y)
y = x.max(axis=1); print(y)
y = x[:2,:2]+x[:2,2:]; print(y)
y = x[:2, :3].T; print(y)
y = x[:2, :3].reshape((3, 2)); print(y)
y=x[:,:2].dot([1, 1]); print(y)
y = x[:, :2].dot([[3, 0], [0, 2]]); print(y)